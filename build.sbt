libraryDependencies ++= Seq(
  "net.spy"       % "spymemcached" % "2.10.1",
  "org.glassfish" % "javax.json"   % "1.0.3"
)

name := "besta"

scalacOptions ++= Seq("-feature")

scalaVersion := "2.10.3"

version := "0.0.0"
