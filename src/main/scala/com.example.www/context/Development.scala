package com.example.www
package context

import besta.adapter.{converter, generator}
import besta.json
import besta.memcached
import java.util.UUID
import net.spy.memcached.{AddrUtil, MemcachedClient}
import post.Posts

object Development extends Context {

  val posts = (
    memcachedStore
      convert  converter.uuid
      generate generator.uuid
      convert  json.converter.jsonObject
      convert  json.converter.map
      convert  converter.values(post.Attributes)
      convert  Posts()
  )

  lazy val memcachedClient =
    new MemcachedClient(AddrUtil getAddresses "localhost:11211")

  lazy val memcachedStore = memcached Store memcachedClient
}
