package com.example.www

import besta.domain.Creatable
import java.util.UUID
import post.Post

trait Context extends besta.Context[Context] {

  val posts: Creatable[UUID, Post]
}
