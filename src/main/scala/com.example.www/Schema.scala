package com.example.www

import besta.Reader

class Schema(context: Context) extends besta.Schema {
  import context._

  val posts = context.posts
}

object Schema extends Reader[Context, Schema](new Schema(_))
