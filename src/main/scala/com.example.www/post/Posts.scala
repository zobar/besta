package com.example.www
package post

import besta.Dao

trait Posts[A] extends Dao[A, Post] {

  def build = Post(_)
}

object Posts {

  def apply[A](): Dao[A, Post] = new Posts[A] {}
}
