package com.example.www
package post

import besta.{Item, Values}

trait Post extends Model {

  type Attributes = Attributes.type

  def name = Attributes name item.data

  def text = Attributes text item.data
}

object Post {

  def apply(i: Item[Values[Attributes.type]]): Post = new Post { val item = i }
}
