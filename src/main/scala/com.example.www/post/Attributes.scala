package com.example
package www.post

object Attributes extends www.Attributes {

  val id   = int("id")

  val name = string("name")

  val text = string("text")
}
