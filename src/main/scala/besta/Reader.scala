package besta

import scala.language.implicitConversions

class Reader[-A, +B] protected (f: A => B) extends (A => B) {

  def apply(memo: A): B = f(memo)

  def map[C](f2: B => C): Reader[A, C] = Reader(memo => f2(f(memo)))

  def flatMap[C <: A, D](f2: B => Reader[C, D]): Reader[C, D] =
    Reader(memo => f2(f(memo))(memo))
}

object Reader {

  implicit def apply[A, B](f: A => B) = new Reader(f)

  def pure[A, B](value: B): Reader[A, B] = Reader(memo => value)
}
