package besta
package adapter

import converter.data._
import converter.id._
import java.util.UUID

package object converter {

  def passthrough[A, B]: Data[A, B, B] = new Passthrough

  def uuid[A]: Id[String, UUID, A] = new Uuid

  def values[A, B <: Attributes](attributes: B): Data[A, TypedMap, besta.Values[B]] =
    new Values(attributes)
}
