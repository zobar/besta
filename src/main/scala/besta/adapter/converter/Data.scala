package besta.adapter
package converter

trait Data[A, B, C] extends Converter[A, B, A, C] {

  def decodePersisted(item: UnderlyingPersistedItem) =
    item withData decodeData(item.data)

  def encode(item: Item) = item withData encodeData(item.data)

  def encodeId(id: Id) = id

  def encodePersisted(item: PersistedItem) =
    item withData encodeData(item.data)

  protected[this] def decodeData(data: UnderlyingData): Data

  protected[this] def encodeData(data: Data): UnderlyingData
}
