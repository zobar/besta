package besta.adapter
package converter

trait Id[A, B, C] extends Converter[A, C, B, C] {

  def decodePersisted(item: UnderlyingPersistedItem): PersistedItem =
    item withId decodeId(item.id)

  def encode(item: Item): UnderlyingItem = item

  def encodePersisted(item: PersistedItem): UnderlyingPersistedItem =
    item withId encodeId(item.id)

  protected[this] def decodeId(id: UnderlyingId): Id

}
