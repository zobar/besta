package besta.adapter.converter
package data

private[converter] class Passthrough[A, B] extends Data[A, B, B] {

  protected[this] def decodeData(data: UnderlyingData) = data

  protected[this] def encodeData(data: Data) = data
}
