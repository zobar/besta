package besta
package adapter.converter
package data

import typedMap.Conversions._

private[converter] class Values[A, B <: Attributes](attributes: B)
    extends Data[A, TypedMap, besta.Values[B]] {

  protected[this] def decodeData(data: UnderlyingData) =
    besta.Values(attributes, data)

  protected[this] def encodeData(data: Data) = data
}
