package besta.adapter.converter
package id

import java.util.UUID

private[converter] class Uuid[A] extends Id[String, UUID, A] {

  def decodeId(id: UnderlyingId) = UUID fromString id

  def encodeId(id: Id) = id.toString
}
