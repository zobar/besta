package besta.adapter
package generator

import java.util.UUID

private class Uuid[A] extends Generator[UUID, A] {

  def createId() = UUID.randomUUID()
}
