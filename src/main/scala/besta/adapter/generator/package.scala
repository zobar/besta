package besta.adapter

import java.util.UUID

package object generator {

  def uuid[A]: Generator[UUID, A] = new Uuid
}
