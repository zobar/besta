package besta
package adapter

trait Generator[A, B] {

  type Id = A

  type Item = besta.Item[Value]

  type PersistedItem = besta.item.Persisted[Id, Value]

  type Value = B

  def create(item: Item): PersistedItem =
    Item(createId(), item.data, item.metadata)

  protected[this] def createId(): A
}
