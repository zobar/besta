package besta
package adapter

trait Converter[A, B, C, D] {

  type Data = D

  type Id = C

  type Item = besta.Item[Data]

  type PersistedItem = item.Persisted[Id, Data]

  type UnderlyingData = B

  type UnderlyingId = A

  type UnderlyingItem = besta.Item[UnderlyingData]

  type UnderlyingPersistedItem = item.Persisted[UnderlyingId, UnderlyingData]

  def decodePersisted(item: UnderlyingPersistedItem): PersistedItem

  def encode(item: Item): UnderlyingItem

  def encodeId(id: Id): UnderlyingId

  def encodePersisted(item: PersistedItem): UnderlyingPersistedItem
}
