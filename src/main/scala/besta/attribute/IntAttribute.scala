package besta
package attribute

import typedMap.Conversions._

private class IntAttribute(val name: String) extends Attribute[Int] {

  def fetch(values: Map[String, Any]) = values int name
}
