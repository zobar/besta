package besta
package attribute

trait BasicAttributes { self: Attributes =>

  protected[this] def int(name: String): Attribute[Int] =
    attribute(new IntAttribute(name))

  protected[this] def string(name: String): Attribute[String] =
    attribute(new StringAttribute(name))
}
