package besta
package attribute

import typedMap.Conversions._

private class StringAttribute(val name: String) extends Attribute[String] {

  def fetch(values: Map[String, Any]) = values string name
}
