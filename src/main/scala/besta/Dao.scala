package besta

import adapter.Converter

trait Dao[A, B <: Model] extends Converter[A, B#Values, A, B] {

  type Attributes = Data#Attributes

  def build: UnderlyingPersistedItem => Data

  def decodePersisted(item: UnderlyingPersistedItem) = item withData build(item)

  def encode(item: Item) = item withData item.data.values

  def encodePersisted(item: PersistedItem) = item withData item.data.values

  def encodeId(id: Id) = id
}
