package besta
package metadata

import language.implicitConversions

object Conversions {

  implicit def withTimeToLive(metadata: Metadata): TimeToLive = metadata match {
    case timeToLive: TimeToLive => timeToLive
    case _ => new TimeToLive { val timeToLive = None }
  }

  implicit def withTypeFlags(metadata: Metadata): TypeFlags = metadata match {
    case typeFlags: TypeFlags => typeFlags
    case _ => new TypeFlags { val typeFlags = 0 }
  }
}
