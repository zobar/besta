package besta.metadata
import java.util.Date

trait TimeToLive {

  def timeToLive: Option[Date]
}
