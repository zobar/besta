package besta

trait Context[A <: Context[A]] { self: A =>

  def context[B](f: Reader[A, B]): B = f(this)
}
