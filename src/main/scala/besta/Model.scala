package besta

trait Model {

  type Attributes <: besta.Attributes

  type Item = besta.Item[Values]

  type Values = besta.Values[Attributes]

  def item: Item

  def metadata: Metadata = item.metadata

  def values: Values = item.data
}
