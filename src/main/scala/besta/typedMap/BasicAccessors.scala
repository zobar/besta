package besta
package typedMap

import accessor.Required

trait BasicAccessors extends TypedMap {
  self =>

  def int: besta.Accessor[Int] = new Accessor[Int] ({
    case value: Int => value
  })

  def string: besta.Accessor[String] = new Accessor[String] ({
    case value: String => value
  })

  private[this] class Accessor[A](val extract: PartialFunction[Any, A])
      extends Required[A] {

    val map = self
  }
}
