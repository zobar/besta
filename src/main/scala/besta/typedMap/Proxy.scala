package besta
package typedMap

import Conversions._

private class Proxy(underlying: Map[String, Any])
    extends Map[String, Any] with BasicAccessors {

  def +[A >: Any](kv: (String, A)) = underlying + kv

  def -(key: String) = underlying - key

  def get(key: String) = underlying get key

  def iterator = underlying.iterator
}
