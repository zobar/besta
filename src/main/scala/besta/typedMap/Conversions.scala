package besta
package typedMap

import scala.language.implicitConversions

object Conversions {

  implicit def asTypedMap(map: Map[String, Any]): TypedMap = map match {
    case typedMap: TypedMap => typedMap
    case underlying => new Proxy(underlying)
  }
}
