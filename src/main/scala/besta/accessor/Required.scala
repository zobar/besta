package besta
package accessor

import validation.Failure
import validation.failure._

trait Required[A] extends Accessor[A] {

  def apply(name: String): Either[Failure, A] =
    map andThen wrap orElse requiredFailure apply name

  def extract: PartialFunction[Any, A]

  def map: Map[String, Any]

  private[this] val requiredFailure: PartialFunction[String, Left[Failure, A]] =
    { case _ => Left(Required) }

  private[this] val success = { value: A => Right(value) }

  private[this] val typeFailure: PartialFunction[Any, Left[Failure, A]] =
    { case _ => Left(Type) }

  private[this] val wrap = extract andThen success orElse typeFailure
}
