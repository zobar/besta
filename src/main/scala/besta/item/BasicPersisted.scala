package besta
package item

private[besta] class BasicPersisted[A, B](val id: A, data: B, metadata: Metadata)
    extends Basic[B](data, metadata)
    with Persisted[A, B] {

  override def withData[B](data: B) = Item(id, data, metadata)

  def withId[B](id: B) = Item(id, data, metadata)
}
