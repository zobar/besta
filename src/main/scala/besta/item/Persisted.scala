package besta
package item

trait Persisted[A, B] extends Item[B] {

  type Id = A

  def id: Id

  override def withData[C](data: C): Persisted[Id, C]

  def withId[C](id: C): Persisted[C, Data]
}
