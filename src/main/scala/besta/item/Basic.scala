package besta
package item

private[besta] class Basic[A](val data: A, val metadata: Metadata)
    extends Item[A] {

  def withData[B](data: B) = Item(data, metadata)
}
