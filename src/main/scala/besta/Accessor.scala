package besta

import validation.Failure

trait Accessor[A] {

  def apply(name: String): Either[Failure, A]
}
