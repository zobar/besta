package besta

trait TypedMap extends Map[String, Any] {

  def +[A >: Any](kv: (String, A)): TypedMap

  def -(key: String): TypedMap

  def int: Accessor[Int]

  def string: Accessor[String]
}
