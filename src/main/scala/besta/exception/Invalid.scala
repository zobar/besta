package besta
package exception

import validation.Failure

class Invalid(val failure: Failure) extends Exception

object Invalid {
  def apply(failure: Failure): Invalid = new Invalid(failure)
}
