package besta

import validation.Failure
import exception.Invalid

trait Attribute[+A] {

  def apply(map: Map[String, Any]): A = fetch(map) fold (
    failure => throw Invalid(failure),
    value => value
  )

  def fetch(map: Map[String, Any]): Either[Failure, A]

  def get(map: Map[String, Any]): Option[A] = fetch(map).right.toOption

  def name: String
}
