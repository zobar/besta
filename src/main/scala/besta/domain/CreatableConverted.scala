package besta
package domain

import adapter.Converter

private class CreatableConverted[A, B, C, D](converter: Converter[A, B, C, D], underlying: Creatable[A, B])
    extends Converted(converter, underlying)
    with Creatable[C, D] {

  def create(item: Item): PersistedItem =
    converter decodePersisted (underlying create (converter encode item))
}

object CreatableConverted {

  def apply[A, B, C, D](converter: Converter[A, B, C, D], underlying: Creatable[A, B]): Creatable[C, D] =
    new CreatableConverted(converter, underlying)
}
