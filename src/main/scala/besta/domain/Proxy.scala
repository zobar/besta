package besta
package domain

import adapter.Generator

private class Proxy[A, B](underlying: Domain[A, B]) extends Domain[A, B] {

  def delete(id: Id) = underlying delete id

  def read(id: Id): Option[PersistedItem] = underlying read id

  def update(item: PersistedItem): PersistedItem = underlying update item
}
