package besta
package domain

import adapter.Generator

private class Generated[A, B](generator: Generator[A, B], underlying: Domain[A, B])
    extends Proxy(underlying) with Creatable[A, B] {

  def create(item: Item): PersistedItem = update(generator create item)
}

object Generated {

  def apply[A, B](generator: Generator[A, B], underlying: Domain[A, B]): Creatable[A, B] =
    new Generated(generator, underlying)
}
