package besta
package domain

import adapter.Converter

private class Converted[A, B, C, D](converter: Converter[A, B, C, D], underlying: Domain[A, B])
    extends Domain[C, D] {

  def delete(id: Id): Unit = underlying delete (converter encodeId id)

  def read(id: Id): Option[PersistedItem] =
    underlying read (converter encodeId id) map converter.decodePersisted

  def update(item: PersistedItem): PersistedItem =
    converter decodePersisted (underlying update (converter encodePersisted item))
}

object Converted {

  def apply[A, B, C, D](converter: Converter[A, B, C, D], underlying: Domain[A, B]): Domain[C, D] =
    new Converted(converter, underlying)
}
