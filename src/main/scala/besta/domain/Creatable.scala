package besta
package domain

import adapter.Converter

trait Creatable[A, B] extends Domain[A, B] {

  type Item = besta.Item[Data]

  override def convert[C, D](converter: Converter[A, B, C, D]): Creatable[C, D] =
    CreatableConverted(converter, this)

  def create(item: Item): PersistedItem
}
