package besta

private class BasicValues[A <: Attributes](val attributes: A, val underlying: TypedMap)
    extends Values[A] {

  def +[B >: Any](kv: (String, B)) = Values(attributes, underlying + kv)

  def -(key: String) = Values(attributes, underlying - key)

  def get(key: String) = attributes lift key flatMap { _ get underlying }

  def iterator =
    attributes.iterator flatMap { a => a get underlying map { (a.name, _) } }
}
