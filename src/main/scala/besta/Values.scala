package besta

trait Values[+A <: Attributes] extends Map[String, Any] {

  type Attributes = A

  def +[B >: Any](kv: (String, B)): Values[Attributes]

  def attributes: Attributes
}

object Values {

  def apply[A <: Attributes](attributes: A, underlying: TypedMap): Values[A] =
    new BasicValues(attributes, underlying)
}
