package besta

import attribute.BasicAttributes
import scala.collection.mutable.LinkedHashMap

trait Attributes extends BasicAttributes
    with Iterable[Attribute[Any]]
    with PartialFunction[String, Attribute[Any]] {

  def apply(name: String) = attributes(name)

  def isDefinedAt(name: String) = attributes isDefinedAt name

  def iterator = attributes.iterator map { _._2 }

  protected[this] def attribute[A](attribute: Attribute[A]): Attribute[A] = {
    attributes += (attribute.name -> attribute)
    attribute
  }

  private[this] val attributes = LinkedHashMap[String, Attribute[Any]]()
}
