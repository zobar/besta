package besta

import item.{Basic, BasicPersisted, Persisted}

trait Item[+A] {

  type Data = A

  def data: Data

  def metadata: Metadata

  def withData[B](data: B): Item[B]
}

object Item {

  def apply[A, B](id: A, data: B, metadata: Metadata): Persisted[A, B] =
    new BasicPersisted(id, data, metadata)

  def apply[A](data: A, metadata: Metadata): Item[A] = new Basic(data, metadata)
}
