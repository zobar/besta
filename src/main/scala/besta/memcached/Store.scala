package besta
package memcached

import metadata.Conversions._
import net.spy.memcached.MemcachedClient

private class Store(client: MemcachedClient)
    extends besta.Store with Domain[String, Array[Byte]] {

  def delete(id: Id) = client.delete(id).get

  def read(id: Id) = Option(client.get(id, new Transcoder(id)))

  def update(item: PersistedItem) = {
    val ttl = item.metadata.timeToLive map { _.getTime.toInt / 1000 } getOrElse 0
    client.set(item.id, ttl, item, new Transcoder(item.id)).get
    item
  }
}

object Store {
  def apply(client: MemcachedClient): besta.Store with Domain[String, Array[Byte]] =
    new Store(client)
}
