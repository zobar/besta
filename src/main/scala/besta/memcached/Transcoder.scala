package besta
package memcached

import item.Persisted
import metadata.Conversions._
import metadata.TypeFlags
import net.spy.memcached.{CachedData, transcoders}

private class Transcoder(id: String) extends transcoders.Transcoder[Persisted[String, Array[Byte]]] {

  def asyncDecode(d: CachedData) = false

  def decode(d: CachedData) =
    Item(id, d.getData, new Metadata with TypeFlags { val typeFlags = d.getFlags })

  def encode(o: Persisted[String, Array[Byte]]) = new CachedData(o.metadata.typeFlags, o.data, getMaxSize)

  def getMaxSize = CachedData.MAX_SIZE
}
