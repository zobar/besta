package besta.jdbc

class Row extends Map[String, Any] {

  def +[A >: Any](kv: (String, A)): Map[String, A] = ???

  def -(key: String): Map[String, Any] = ???

  def get(key: String) = dummy get key

  def iterator = dummy.iterator

  private[this] val dummy = Map(
    "id"   -> 1,
    "name" -> "My first post!",
    "text" -> "This is my first post."
  )
}
