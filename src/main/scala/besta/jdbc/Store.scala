package besta.jdbc

trait Store extends besta.Store {

  def domain[A](name: String): Table[A] = new Table[A](name)
}
