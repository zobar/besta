package besta
package json

import javax.json.{JsonObject, JsonString, JsonValue}
import scala.collection.immutable.DefaultMap

private class ObjectMap(underlying: JsonObject) extends DefaultMap[String, Any] {

  def get(key: String) = Option(underlying get key) map decode

  def iterator = {
    val iterator = underlying.entrySet.iterator

    new Iterator[(String, Any)] {
      def hasNext = iterator.hasNext

      def next() = {
        val entry = iterator.next()
        (entry.getKey, decode(entry.getValue))
      }
    }
  }

  private[this] def decode(value: JsonValue) = value match {
    case s: JsonString => s.getString
  }
}

private object ObjectMap {

  def apply(underlying: JsonObject): TypedMap = Map(ObjectMap(underlying))
}
