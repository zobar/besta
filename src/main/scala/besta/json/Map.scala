package besta
package json

import scala.collection.immutable

private class Map(underlying: immutable.Map[String, Any]) extends Accessors {

  def +[A >: Any](kv: (String, A)) = Map(underlying + kv)

  def -(key: String) = Map(underlying - key)

  def get(key: String) = underlying get key

  def iterator = underlying.iterator
}

private object Map {

  def apply(underlying: immutable.Map[String, Any]): TypedMap =
    new Map(underlying)
}
