package besta
package json

import adapter.converter.Data
import javax.json.JsonObject

package object converter {

  def map[A]: Data[A, JsonObject, TypedMap] = new Map

  def jsonObject[A]: Data[A, Array[Byte], JsonObject] = new Object
}
