package besta
package json
package converter

import adapter.converter.Data
import javax.json.{Json, JsonObject}
import Map._

private class Map[A] extends Data[A, JsonObject, TypedMap] {

  def decodeData(data: UnderlyingData) = ObjectMap(data)

  def encodeData(data: Data) =
    (builderFactory.createObjectBuilder /: data) {
      case (builder, (key, value: String)) => builder.add(key, value)
    }.build
}

object Map {

  private val builderFactory = Json createBuilderFactory null
}
