package besta
package json
package converter

import adapter.converter.Data
import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import javax.json.{Json, JsonObject}
import Object._

private class Object[A] extends Data[A, Array[Byte], JsonObject] {

  def decodeData(data: UnderlyingData) = {
    val inputStream = new ByteArrayInputStream(data)
    val jsonReader = readerFactory createReader inputStream
    val jsonObject = jsonReader.readObject

    jsonReader.close()
    inputStream.close()
    jsonObject
  }

  def encodeData(data: Data) = {
    val outputStream = new ByteArrayOutputStream
    val jsonWriter = writerFactory createWriter outputStream

    jsonWriter writeObject data
    jsonWriter.close()
    outputStream.close()
    outputStream.toByteArray
  }
}

object Object {

  private val readerFactory = Json createReaderFactory null

  private val writerFactory = Json createWriterFactory null
}
