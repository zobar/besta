package besta

import adapter.{Converter, Generator}
import adapter.converter.passthrough
import domain.{Converted, Creatable, Generated}
import item.Persisted

trait Domain[A, B] {

  type Data = B

  type Id = A

  type PersistedItem = Persisted[Id, Data]

  def convert[C, D](converter: Converter[A, B, C, D]): Domain[C, D] =
    Converted(converter, this)

  def delete(id: Id): Unit

  def generate(generator: Generator[A, B]): Creatable[A, B] =
    Generated(generator, this)

  def read(id: Id): Option[PersistedItem]

  def update(item: PersistedItem): PersistedItem

}
